#!/bin/env python3

import pulsectl

if __name__ == '__main__':
    p = pulsectl.Pulse("listing")
    print("SOURCES:")
    for source in p.source_list():
        print(f"{source.name:<65} {source.description}")
    print()
    print()
    print("SINKS:")
    for sink in p.sink_list():
        print(f"{sink.name:<65} {sink.description}")

    print()
    print()
    print("SINK INPUTS:")
    for input in p.sink_input_list():
        print(f"{input}")
        print(input.proplist)

    print()
    print()
    print("SOURCE OUTPUTS:")
    for output in p.source_output_list():
        print(f"{output}")
        print(output.proplist)
