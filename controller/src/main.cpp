#include <Arduino.h>

struct input_pin_status {
    uint8_t pin;
    bool last_state;
    bool in_change;
    uint32_t last_change;
};

input_pin_status input_status[] = {
        {9, false, false, 0},
        {10, false, false, 0},
        {11, false, false, 0},
        {12, false, false, 0},
};

#define DEBUG_LED_PIN 13


uint32_t last_mic_request = 0;
#define MIC_RESPONSE_LENGTH_WITH_LF 7

void setup() {
    Serial.begin(115200);
    pinMode(DEBUG_LED_PIN, OUTPUT);
    for (auto input : input_status) {
        pinMode(input.pin, INPUT_PULLUP);
    }
}

void loop() {
    for (auto & input : input_status) {
        bool state = digitalRead(input.pin);
        if (input.last_state != state) {
            input.last_state = state;
            input.last_change = millis();
            input.in_change = true;
        }
        if (input.in_change && ((millis() - input.last_change) > 100)) {
            switch (input.pin) {
                case 12:
                    (input.last_state ? Serial.print("PS+RLR\n") : Serial.print("PS+CFV\n"));
                    break;
                case 11:
                    (input.last_state ? Serial.print("PS+S\n") : Serial.print("PS+H\n"));
                    break;
                case 10:
                    (input.last_state ? Serial.print("PS+M=1\n") : Serial.print("PS+M=0\n"));
                    break;
                case 9:
                    (input.last_state ? Serial.print("PS+SOP\n") : Serial.print("PS+SAP\n"));
                    break;
            }
            input.in_change = false;
        }
    }

    if ((millis() - last_mic_request) > 50) {
        Serial.print("PS+M=?\n");
        last_mic_request = millis();
    }

    while (Serial.available() >= MIC_RESPONSE_LENGTH_WITH_LF) {
        uint8_t buffer[MIC_RESPONSE_LENGTH_WITH_LF];
        Serial.readBytes(buffer, MIC_RESPONSE_LENGTH_WITH_LF);
        if (memcmp(buffer, "PS+M=", 5) == 0) {
            int8_t state = buffer[5] - '0';
            switch (state) {
                case 0:
                    digitalWrite(DEBUG_LED_PIN, true);
                    delay(5);
                    digitalWrite(DEBUG_LED_PIN, false);
                    break;
                case 1:
                    digitalWrite(DEBUG_LED_PIN, false);
                    delay(40);
                    digitalWrite(DEBUG_LED_PIN, true);
                    break;
                case 2:
                    digitalWrite(DEBUG_LED_PIN, true);
                    delay(20);
                    digitalWrite(DEBUG_LED_PIN, false);
                    delay(60);
                    digitalWrite(DEBUG_LED_PIN, true);
                    delay(20);
                    digitalWrite(DEBUG_LED_PIN, false);
                    break;
            }
        } else {
            // empty buffer on corrupted command
            while (Serial.available() > 0) Serial.read();
        }
    }

    delay(20);

    /*
    digitalWrite(13, true);
    delay(800);
    digitalWrite(13, false);
    delay(400);
    Serial.println("testing");
     */
}
