# Pulseaudio Switchboard Operator

Switch easy and sensible between sources, streams and sinks. 

Online Conferences are no fun, but switching audio sources and sinks between speaker and headset is even more annoying, so lets make it more sensible.

# Features

* uses two audio interfaces, speaker (incl. room microphone) and headset
* switch all outputs between both interfaces
* global PTT
* distribute audio outputs between channels
	* you can listen way better to two simultaneously audio source (eg live stream and audio chat) if they are played on different ears, try it!
* detect and handle pulseeffects

# Usage

Create a virtual environment and install dependencies (it is just `pulsectl`)

```bash
pip install -r requirements.txt
```

Customize config file. Use `tool/list_sink_sources.py` for listing of names.

Start TUI with:

```bash
python -m pasbo data/example.ini
```

Start GUI with:

```bash
python -m pasbo.gui data/example.ini
```


# Command Reference

In server mode, remote control over a bidirectional stream interface (TCP, UART, …) is possible.

The syntax of the command set is loosely based on the Hayes AT-Commands.

A command has the prefix `PS` followed by a `+` to indicate a action 
and then the action with optional parameters `<ACTION>[=<VALUE>]`. 
A `?` as value indicates a request of the current state.

Each command is terminated with a linefeed (ASCII `0x0A`).

```
PS+<ACTION>
PS+<ACTION>=?
PS+<ACTION>=<VALUE>
```

## Switch to Speaker

```
PS+S
```

Calls `switch_to_speaker`

## Switch to Headset

```
PS+H
```

Calls `switch_to_headset`

## Distribute streams (random left right)

```
PS+RLR
```

Calls `distribute_inputs_randomly_on_stereo`

## Set all channels to full volumes

Reversion of `PS+RLR`

```
PS+CFV
```

Calls `set_all_inputs_to_full_volume`

## Mute Mics

```
PS+M=1
```

Calls `mute_all_sources`

## Unmute Mics

```
PS+M=0
```

Calls `unmute_all_sources`

## Get Mute State

```
PS+M=?
```

Calls `get_mute_state` and answers with

```
PS+M=<VALUE>
```

where `0` means not muted (source active), `1` means muted 
and `2` means that the state is ambiguous (e.g. there are different sources active and there are in different states).

## Start PTT

```
PS+SAP
```

Calls `start_ptt`

## Stop PTT

```
PS+SOP
```

Calls `stop_ptt`
