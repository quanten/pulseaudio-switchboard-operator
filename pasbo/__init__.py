#!/bin/env python3

import pulsectl
import random
import configparser


class Pasbo:

    pulseeffects_sink_name = "PulseEffects_apps"

    def __init__(self, config_filename, client_name="pasbo"):
        self.mute_state_before_ptt = {}

        self.pulse = pulsectl.Pulse(client_name)
        self.config = configparser.ConfigParser()
        self.config.read(config_filename)

        self.restore_after_ptt = self.config["PASBO"].getboolean("restore-after-ptt")

    def switch_to_headset(self):
        self._default_set_with_none_handling(self._get_headset_source())
        self._default_set_with_none_handling(self._get_headset_sink())
        if self._is_pulseeffects_active():
            self._move_pulseeffects(self._get_headset_source(), self._get_headset_sink())
        else:
            self._move_inputs(self._get_normal_sink_input_list(), self._get_headset_sink())
            self._move_outputs(self._get_normal_source_output_list(), self._get_headset_source())

    def switch_to_speaker(self):
        self._default_set_with_none_handling(self._get_speaker_source())
        self._default_set_with_none_handling(self._get_speaker_sink())
        if self._is_pulseeffects_active():
            self._move_pulseeffects(self._get_speaker_source(), self._get_speaker_sink())
        else:
            self._move_inputs(self._get_normal_sink_input_list(), self._get_speaker_sink())
            self._move_outputs(self._get_normal_source_output_list(), self._get_speaker_source())

    def balance_all_inputs(self):
        for input in self._get_normal_sink_input_list():
            volume = input.volume
            new_volume = pulsectl.PulseVolumeInfo(volume.value_flat, len(volume.values))
            self.pulse.volume_set(input, new_volume)

    def set_all_inputs_to_full_volume(self):
        for input in self._get_normal_sink_input_list():
            volume = input.volume
            new_volume = pulsectl.PulseVolumeInfo(1.0, len(volume.values))
            self.pulse.volume_set(input, new_volume)

    def distribute_inputs_randomly_on_stereo(self):
        shuffled = self._get_normal_sink_input_list()
        random.shuffle(shuffled)
        volume_list = [0.0, 1.0]
        random.shuffle(volume_list)
        one_side = pulsectl.PulseVolumeInfo(volume_list)
        other_side = pulsectl.PulseVolumeInfo(volume_list[::-1])
        for i in range(len(shuffled)):
            self.pulse.volume_set(shuffled[i], one_side if i < len(shuffled) / 2 else other_side)

    def start_ptt(self):
        self.mute_state_before_ptt = { x.index: x.mute for x in self._get_normal_source_list()}
        self._set_mute_state(self._get_normal_source_list(), False)

    def stop_ptt(self):
        if self.restore_after_ptt:
            for i in self.mute_state_before_ptt:
                self.pulse.source_mute(i, self.mute_state_before_ptt[i])
        else:
            self._set_mute_state(self._get_normal_source_list(), True)

    def mute_all_sources(self):
        self._set_mute_state(self._get_normal_source_list(), True)

    def unmute_all_sources(self):
        self._set_mute_state(self._get_normal_source_list(), False)

    def get_mute_state(self) -> int:
        sources = self._get_normal_source_list()
        if len(sources) > 0:
            last = sources[0].mute
            for source in sources[1:]:
                if source.mute != last:
                    return 2
                last = source.mute
            return last
        else:
            # if there is no source, no audio can be recorded, equal to muted
            return 1

    def _default_set_with_none_handling(self, obj):
        if obj is not None:
            self.pulse.default_set(obj)

    def _set_mute_state(self, object_list, value):
        for object in object_list:
            self.pulse.mute(object, value)

    def _move_inputs(self, inputs, target_source):
        if target_source is not None:
            for input in inputs:
                self.pulse.sink_input_move(input.index, target_source.index)

    def _move_outputs(self, outputs, target_sink):
        if target_sink is not None:
            for output in outputs:
                self.pulse.source_output_move(output.index, target_sink.index)

    def _move_pulseeffects(self, target_source, target_sink):
        if target_source is not None:
            source_output = list(filter(lambda obj: "application.id" in obj.proplist and obj.proplist["application.id"] == "com.github.wwmm.pulseeffects.sourceoutputs", self.pulse.source_output_list()))
            if len(source_output) == 1:
                self.pulse.source_output_move(source_output[0].index, target_source.index)
        if target_sink is not None:
            sink_input = list(filter(lambda obj: "application.id" in obj.proplist and obj.proplist["application.id"] == "com.github.wwmm.pulseeffects.sinkinputs", self.pulse.sink_input_list()))
            if len(sink_input) == 1:
                self.pulse.sink_input_move(sink_input[0].index, target_sink.index)

    def _get_headset_source(self):
        return self._get_source_by_name(self.config["headset"]["source-name"])

    def _get_speaker_source(self):
        return self._get_source_by_name(self.config["speaker"]["source-name"])

    def _get_headset_sink(self):
        return self._get_sink_by_name(self.config["headset"]["sink-name"])

    def _get_speaker_sink(self):
        return self._get_sink_by_name(self.config["speaker"]["sink-name"])

    def _get_pulseeffects_source(self):
        return self._get_source_by_name("PulseEffects_mic.monitor")

    def _get_pulseeffects_sink(self):
        return self._get_sink_by_name("PulseEffects_apps")

    def _get_normal_source_output_list(self):
        source_output_list = self.pulse.source_output_list()
        if self._is_pulseeffects_active():
            return list(filter(lambda x: not self._is_obj_from_pulseeffects(x), source_output_list))
        # Filter for pavucontrol and PlasmaPA
        return list(filter(lambda x: x.name != "Peak detect" and not x.name.startswith("PlasmaPA-"), source_output_list))

    def _get_normal_sink_input_list(self):
        if self._is_pulseeffects_active():
            return list(filter(lambda x: not self._is_obj_from_pulseeffects(x), self.pulse.sink_input_list()))
        else:
            return self.pulse.sink_input_list()

    def _get_normal_source_list(self):
        if self._is_pulseeffects_active():
            sources = list(filter(lambda x: not self._is_obj_from_pulseeffects(x), self.pulse.source_list()))
        else:
            sources = self.pulse.source_list()
        return list(filter(lambda x: x.monitor_of_sink_name is None, sources))

    def _get_source_by_name(self, name):
        try:
            source = self.pulse.get_source_by_name(name)
        except pulsectl.pulsectl.PulseIndexError:
            source = None
        return source

    def _get_sink_by_name(self, name):
        try:
            sink = self.pulse.get_sink_by_name(name)
        except pulsectl.pulsectl.PulseIndexError:
            sink = None
        return sink

    def _is_pulseeffects_active(self):
        return self._get_sink_by_name(self.pulseeffects_sink_name) is not None
    
    def _is_obj_from_pulseeffects(self, obj):
        if "application.id" in obj.proplist and obj.proplist["application.id"].startswith("com.github.wwmm.pulseeffects"):
            return True
        if obj.name.startswith("PulseEffects"):
            return True
        return False

