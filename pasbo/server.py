#!/bin/env python

import serial
import argparse

from pasbo import Pasbo

MIN_COMMAND_LENGTH = 5

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("config", help="Path to config file")
    parser.add_argument("serialport", help="Serial Port to use")
    parser.add_argument("-b","--baudrate", help="Baudrate to use, defaults to 115200", default=115200)
    parser.add_argument("-v", "--verbose", help="show executed commands", action="store_true")
    args = parser.parse_args()
    pasbo = Pasbo(args.config, client_name="pasbo-serial-server")

    def verbose_print(content):
        if args.verbose:
            print(content)

    direct_actions = {
        b'S': pasbo.switch_to_speaker,
        b'H': pasbo.switch_to_headset,
        b'RLR': pasbo.distribute_inputs_randomly_on_stereo,
        b'CFV': pasbo.set_all_inputs_to_full_volume,
        b'SAP': pasbo.start_ptt,
        b'SOP': pasbo.stop_ptt
    }

    ser = serial.Serial()
    ser.port = args.serialport
    ser.baudrate = args.baudrate
    ser.timeout = 0.01
    ser.open()
    rx_buffer = bytearray()
    while True:
        try:
            rx_buffer += ser.read(size=10)
        except KeyboardInterrupt:
            break
        while (not rx_buffer.startswith(b'PS+')) and len(rx_buffer) >= MIN_COMMAND_LENGTH:
            rx_buffer = rx_buffer[1:]
        if rx_buffer.startswith(b'PS+') and b'\n' in rx_buffer:
            rx_buffer = rx_buffer[3:]
            cmd = bytearray()
            while rx_buffer[:1] != b'=' and rx_buffer[:1] != b'\n':
                cmd += rx_buffer[:1]
                rx_buffer = rx_buffer[1:]
            if rx_buffer[:1] == b'\n':
                rx_buffer = rx_buffer[1:]
                if bytes(cmd) in direct_actions:
                    verbose_print(cmd.decode())
                    direct_actions[bytes(cmd)]()
                else:
                    print("unknown direct action!")
            elif rx_buffer[:1] == b'=':
                rx_buffer = rx_buffer[1:]
                if cmd == b'M':
                    parameter = rx_buffer[:1]
                    rx_buffer = rx_buffer[1:]
                    if rx_buffer[:1] == b'\n':
                        rx_buffer = rx_buffer[1:]
                        if parameter == b'1':
                            verbose_print("M=1")
                            pasbo.mute_all_sources()
                        elif parameter == b'0':
                            verbose_print("M=0")
                            pasbo.unmute_all_sources()
                        elif parameter == b'?':
                            ser.write(b'PS+M=' + str(pasbo.get_mute_state()).encode() + b'\n')
                        else:
                            print("unkown parameter for mute!")
                    else:
                        print("parameter to long for mute!")
                else:
                    print("unknown parametic action!")
    ser.close()






