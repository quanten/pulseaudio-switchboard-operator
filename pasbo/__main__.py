#!/bin/env python3

import argparse

from pasbo import Pasbo

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("config")
    parser.add_argument("-c", "--cmd", help="Command to execute and then exit")
    args = parser.parse_args()
    pasbo = Pasbo(args.config)
    ptt_active = False
    finished = False
    while not finished:
        try:
            if args.cmd is None:
                cmd = input("> ")
            else:
                cmd = args.cmd
                finished = True
        except KeyboardInterrupt:
            break
        except EOFError:
            break
        if cmd == "sh":
            pasbo.switch_to_headset()
        elif cmd == "ss":
            pasbo.switch_to_speaker()
        elif cmd == "di":
            pasbo.distribute_inputs_randomly_on_stereo()
        elif cmd == "sv":
            pasbo.set_all_inputs_to_full_volume()
        elif cmd == "p":
            if ptt_active:
                pasbo.stop_ptt()
                print("mic deactivated")
            else:
                pasbo.start_ptt()
                print("mic activated")
            ptt_active = not ptt_active
        elif cmd == "de":
            print("pulseeffects", "is" if pasbo._is_pulseeffects_active() else "is not", "active")
        else:
            print("sh: switch to headset")
            print("ss: switch to speaker")
            print("di: distribute inputs on channels")
            print("sv: set all inputs to full volume on both channels")
            print("p:  toggle ptt")
            print("de: detect pulseeffects")

    print()