#!/bin/env python3

import tkinter as tk
import argparse

from pasbo import Pasbo

class PasboGUI(tk.Frame):
    def __init__(self, config_filename, root=None):
        super().__init__(root)
        self.root = root
        self.root.title("Pasbo")
        self.pack()
        self.pasbo = Pasbo(config_filename)
        self.create_widgets()

    def create_widgets(self):
        self.frame_switcher = tk.LabelFrame(self, text="Switching")
        self.frame_switcher.pack()
        self.button_to_headset = tk.Button(self.frame_switcher, text="Switch to Headset", command=lambda : self.pasbo.switch_to_headset())
        self.button_to_speaker = tk.Button(self.frame_switcher, text="Switch to Speaker", command=lambda : self.pasbo.switch_to_speaker())
        self.button_to_headset.pack()
        self.button_to_speaker.pack()

        self.frame_mic = tk.LabelFrame(self, text="Mic")
        self.frame_mic.pack()
        self.button_ptt = tk.Button(self.frame_mic, text="PTT")
        self.button_ptt.bind("<Button-1>", lambda x: self.pasbo.start_ptt())
        self.button_ptt.bind("<ButtonRelease-1>", lambda x : self.pasbo.stop_ptt())
        self.button_ptt.pack()
        self.button_mute_mics = tk.Button(self.frame_mic, text="Mute Mics", command=self.pasbo.mute_all_sources)
        self.button_mute_mics.pack()
        self.button_unmute_mics = tk.Button(self.frame_mic, text="Unmute Mics", command=self.pasbo.unmute_all_sources)
        self.button_unmute_mics.pack()

        self.frame_channels = tk.LabelFrame(self, text="Channels")
        self.frame_channels.pack()
        self.button_distribute_across_stereo = tk.Button(self.frame_channels, text="Random Left/Right", command=self.pasbo.distribute_inputs_randomly_on_stereo)
        self.button_distribute_across_stereo.pack()
        self.button_reset_stereo_volume = tk.Button(self.frame_channels, text="Reset Channels", command=self.pasbo.set_all_inputs_to_full_volume)
        self.button_reset_stereo_volume.pack()

    def _create_button(self, text, command):
        button = tk.Button(self)
        button["text"] = text
        button["command"] = command
        return button


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("config")
    args = parser.parse_args()
    root = tk.Tk()
    app = PasboGUI(args.config, root=root)
    app.mainloop()